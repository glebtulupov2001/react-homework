import React, { Component } from "react";
import Buttons from "./components/Buttons";
import ButtonFirst from "./components/Buttons/components/ButtonFirst";
import ModalF from "./components/ModalF";
import ModalS from "./components/ModalS";
import "./App.scss";

export default class App extends React.Component {
  state = {
    bgColorF: "blue",
    bgColorS: "yellow",
    textF: "Open first modal",
    textS: "Open second modal",
    isModalF: false,
    isModalS: false,
    modalFHeader: "This is first modal",
    modalFText: "Hello, everybody",
    modalSHeader: "This is second modal",
    modalSText: "Hello, everybody from second modal",
  };

  handleModalF = () => {
    this.setState({ isModalF: !this.state.isModalF });
    console.log(this.state.isModalF);
  };

  handleModalS = () => {
    this.setState({ isModalS: !this.state.isModalS });
    console.log(this.state.isModalS);
  };

  outsideClick = (event) => {
    event.preventDefault();
    const modalWindow = document.querySelector(".modal");
    if (event.target === modalWindow) {
      this.setState({ isModalF: false })
      this.setState({ isModalS: false })
    }
  };

  render() {
    return (
      <div onClick={this.outsideClick}>
        <Buttons
          bgColorF={this.state.bgColorF}
          bgColorS={this.state.bgColorS}
          textF={this.state.textF}
          textS={this.state.textS}
          onClickF={this.handleModalF}
          onClickS={this.handleModalS}
        />
        {this.state.isModalF && (
          <ModalF
            actions={
              <Buttons
                textF={"Agree"}
                textS={"Disagree"}
                onClickF={this.handleModalF}
                onClickS={this.handleModalF}
              />
            }
            header={this.state.modalFHeader}
            text={this.state.modalFText}
            closeButton={this.handleModalF}
          />
        )}
        {this.state.isModalS && (
          <ModalS
            actions={
              <Buttons
                textF={"OK"}
                textS={"NO"}
                onClickF={this.handleModalS}
                onClickS={this.handleModalS}
              />
            }
            header={this.state.modalSHeader}
            text={this.state.modalSText}
            closeButton={this.handleModalS}
          />
        )}
      </div>
    );
  }
}
