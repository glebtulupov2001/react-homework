import React from "react";
import ButtonSecond from "./components/ButtonSecond";
import ButtonFirst from "./components/ButtonFirst";

export default class Buttons extends React.Component {
  render() {
    const { bgColorF, bgColorS, textF, textS, onClickF, onClickS } = this.props;
    return (
      <React.Fragment>
        <ButtonFirst bgColor={bgColorF} text={textF} onClick={onClickF}/>
        <ButtonSecond bgColor={bgColorS} text={textS} onClick={onClickS}/>
      </React.Fragment>
    );
  }
}
