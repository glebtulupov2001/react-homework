import React from "react";

export default class ButtonFirst extends React.Component {
  render() {
    const { bgColor, text, onClick } = this.props;
    return <button style={{ backgroundColor: bgColor }} onClick={onClick}>{text}</button>;
  }
}
